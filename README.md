# Hello!

This repo used to contains merge team related scripts and the old
version of Tezos Coredev Dashboard (aka Arvid2k).

Now it only serves to add a redirect users from old location of the
dashboard (https://arvidnl.gitlab.io/tezos-merge-team-scripts/) to the
new location (https://a2k.nomadic-labs.com/) using GitLab Pages.

The merge team related scripts can now be found here: https://gitlab.com/arvidnl/tezos_merge_team_scripts.

The repo for Arvid2k is here: https://gitlab.com/arvidnl/tezos_merge_team_scripts.

